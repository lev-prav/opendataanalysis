﻿namespace OpenDataAnalysis.Models.ViewModel
{
    public class InfoByIndicatorMetadata
    {
        public string IndicatorId { get; set; }
        public string IndicatorDescription { get; set; }
        public int Total { get; set; }
        public InfoByIndicatorMetadata(string indicatorId, string indicatorDescription, int total)
        {
            IndicatorId = indicatorId;
            IndicatorDescription = indicatorDescription;
            Total = total;
        }
    }
}
