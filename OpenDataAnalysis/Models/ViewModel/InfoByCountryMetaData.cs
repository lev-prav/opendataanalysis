﻿namespace OpenDataAnalysis.Models.ViewModel
{
    public class InfoByCountryMetaData
    {
        public string CountryId { get; set; }
        public string CountryDescription { get; set; }
        public int Total { get; set; }
        public InfoByCountryMetaData(string countryId, string countryDescription, int total)
        {
            CountryId = countryId;
            CountryDescription = countryDescription;
            Total = total;
        }
    }
}
