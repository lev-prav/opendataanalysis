﻿namespace OpenDataAnalysis.Models.ViewModel
{
    public class InfoByCountryViewModel
    {
        public string IndicatorId { get; set; }
        public string IndicatorName { get; set; }
        public List<YearValue> Values { get; set; }
        public InfoByCountryViewModel(string indicatorId, string indicatorName, List<YearValue> values)
        {
            IndicatorId = indicatorId;
            IndicatorName = indicatorName;
            Values = values;
        }
    }

    public class YearValue
    {
        public int Year { get; set; }
        public decimal? Value { get; set; }
    }
}