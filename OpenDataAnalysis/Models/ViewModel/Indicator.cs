﻿namespace OpenDataAnalysis.Models.ViewModel
{
    public class IndicatorViewModel
    {
        public string IndicatorId { get; set; }
        public string Description { get; set; }
        public IndicatorViewModel(string indicatorId, string description)
        {
            IndicatorId = indicatorId;
            Description = description;
        }
    }

}
