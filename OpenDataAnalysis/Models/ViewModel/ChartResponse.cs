﻿namespace OpenDataAnalysis.Models.ViewModel
{
    public class ChartResponse<TResponseMetadataClass, TResponseBodyClass>
    {
        public TResponseMetadataClass ResponseMetadata { get; set; }
        public List<TResponseBodyClass> ResponseBody { get; set; }
        public ChartResponse(TResponseMetadataClass responseMetadata, List<TResponseBodyClass> responseBody)
        {
            ResponseMetadata = responseMetadata;
            ResponseBody = responseBody;
        }
    }
}