﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Net;

namespace OpenDataAnalysis.Models
{
    
    public class ApiError : ObjectResult, IActionResult
    {
        public new HttpStatusCode? StatusCode { get; private set; }
        public string Description { get; private set; }

        
        public ApiError(HttpStatusCode statusCode,Exception value)
            : base(value)
        {
            StatusCode = statusCode;
            Description = value.Message;
        }

        public string JsonResponse()
        {
            var json = JsonConvert.SerializeObject(new
            {
                statusCode = StatusCode,
                description = Description
            });

            return json;
        }

    }
}
