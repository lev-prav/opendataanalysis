﻿namespace OpenDataAnalysis.Models.ViewModel
{
    public class CountryViewModel
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public CountryViewModel(string code, string name)
        {
            Code = code;
            Name = name;
        }
    }

}
