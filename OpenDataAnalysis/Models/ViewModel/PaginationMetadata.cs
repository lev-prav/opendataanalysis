﻿namespace OpenDataAnalysis.Models.ViewModel
{
    public class PaginationMetadata
    {
        /// <summary>
        /// Current page number
        /// </summary>
        public int Page { get; set; }
        /// <summary>
        /// Total number of pages for the request
        /// </summary>
        public int Pages { get; set; }
        /// <summary>
        /// Number of records by page
        /// </summary>
        public int PerPage { get; set; }
        /// <summary>
        /// Total number of records for the request
        /// </summary>
        public int Total { get; set; }

        public PaginationMetadata(int total, int page, int perPage)
        {
            Page = page;
            Pages = (int)Math.Ceiling((double)total / perPage);
            PerPage = perPage;
            Total = total;
        }
    }
}
