﻿namespace OpenDataAnalysis.Models.ViewModel
{
    public class InfoByIndicatorViewModel
    {
        public string CountryId { get; set; }
        public string CountryName { get; set; }
        public List<YearValue> Values { get; set; }
        public InfoByIndicatorViewModel(string countryId, string countryName, List<YearValue> values)
        {
            CountryId = countryId;
            CountryName = countryName;
            Values = values;
        }
    }
}
