using System.Reflection;
using OpenDataServiseLib;

var builder = WebApplication.CreateBuilder(args);
builder.Configuration.AddJsonFile($"appsettings__{Environment.UserName}.json", true);
IConfiguration configuration = builder.Configuration;

// Add services to the container.
builder.Services.AddControllersWithViews();
builder.Services.AddCors();

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
{
    var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.XML";
    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
    options.IncludeXmlComments(xmlPath);
});

builder.Services.AddFlsContext(configuration.GetConnectionString("Connection"));

var app = builder.Build();


// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.EnsureDBCreated();
    //TODO: app.EnsureDBUpdated();    
    app.UseDeveloperExceptionPage();
    string swaggerBasePath = "api";
    app.UseSwagger(c =>
    {
        c.RouteTemplate = swaggerBasePath + "/swagger/{documentName}/swagger.json";
    });
    app.UseSwaggerUI(c =>
    {
        c.SwaggerEndpoint($"/{swaggerBasePath}/swagger/v1/swagger.json", "OpenDataAnalysis v1");
        c.RoutePrefix = $"{swaggerBasePath}/swagger";
    });
}
else
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseDefaultFiles();
app.UseStaticFiles();

app.UseRouting();
app.UseCors(builder => builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();