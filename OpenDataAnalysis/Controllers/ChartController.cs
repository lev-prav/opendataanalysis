﻿using System.Diagnostics;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using OpenDataAnalysis.Models;
using OpenDataAnalysis.Models.ViewModel;
using Microsoft.EntityFrameworkCore;
using OpenDataServiseLib.Models;
using OpenDataServiseLib;

namespace OpenDataAnalysis.Controllers;


[ApiController]
[Route("api")]
public class ChartController : Controller
{
    private readonly IDataBaseWorker databaseWorker;
    private readonly uint perPageMax = 100; // Change XML commentaries for methods if you change this value
    public ChartController(IDataBaseWorker databaseWorker)
    {
        this.databaseWorker = databaseWorker;
    }

    /// <summary>
    /// Возвращает страны
    /// </summary>
    /// <param name="page">Страница</param>
    /// <param name="perPage">Количество записей на странице</param>
    /// <param name="countryNamePart">Часть названия стран, которые будут выведены</param>
    /// <remarks>
    /// Отсчет страниц начинается с 1. Т.е. при (page = 1, perPage = 50) вернутся первые 50 записей
    /// 
    /// Не больше 100 записей на страницу
    /// </remarks>
    /// <response code = "200"> Успешно </response> 
    /// <response code = "204"> Пустой ответ (необходимая информация отсутствует в БД)</response>   
    /// <response code = "503"> Ошибка подключения к базе данных  </response> 
    [Route("country")]
    [HttpGet]
    public async Task<ActionResult<ChartResponse<PaginationMetadata, CountryCount>>> CountriesAsync(
        int page = 1, int perPage = 50, string? countryNamePart = "")
    {
        try
        {
            CheckPageRangeThrowable(page, perPage);
            countryNamePart ??= "";
            var dbResult = await databaseWorker.GetCountriesAsync(page, perPage, countryNamePart);
            if (dbResult.Count == 0) return NoContent();

            var responseMetadata = new PaginationMetadata(await databaseWorker.GetCountriesCountAsync(countryNamePart),
                page, perPage);
            
            var answer =
                new ChartResponse<PaginationMetadata, CountryCount>(responseMetadata, dbResult);

            return Ok(answer);
        }
        catch (ArgumentException ex)
        {
            return UnprocessableEntity(ex.Message);
        }
        catch (Exception ex)
        {
            var error = new ApiError(HttpStatusCode.ServiceUnavailable,ex);
            return new ContentResult { Content = error.JsonResponse(),StatusCode = (int?)error.StatusCode };            
        }
        
    }

    /// <summary>
    /// Возвращает индикаторы
    /// </summary>
    /// <param name="page">Страница</param>
    /// <param name="perPage">Количество записей на странице</param>
    /// <param name="indicatorNamePart">Часть названия индикаторов, которые будут выведены</param>
    /// <remarks>
    /// Отсчет страниц начинается с 1. Т.е. при (page = 1, perPage = 50) вернутся первые 50 записей
    /// 
    /// Не больше 100 записей на страницу
    /// </remarks>
    /// <response code = "200"> Успешно </response> 
    /// <response code = "204"> Пустой ответ (необходимая информация отсутствует в БД)</response> 
    /// <response code = "503"> Ошибка подключения к базе данных  </response> 
    [Route("indicators")]
    [HttpGet]
    public async Task<ActionResult<ChartResponse<PaginationMetadata, IndicatorCount>>> IndicatorsAsync(
        int page = 1, int perPage = 50, string? indicatorNamePart = "")
    {
        try
        {
            CheckPageRangeThrowable(page, perPage);
            indicatorNamePart ??= "";
            var dbResult = await databaseWorker.GetIndicatorsAsync(page, perPage, indicatorNamePart);
            if (dbResult.Count == 0) return NoContent();

            var responseMetadata = new PaginationMetadata(await databaseWorker.GetIndicatorsCountAsync(indicatorNamePart), page, perPage);

            var answer = new ChartResponse<PaginationMetadata, IndicatorCount>(responseMetadata, dbResult);

            return Ok(answer);
        }
        catch (Exception ex)
        {
            var error = new ApiError(HttpStatusCode.ServiceUnavailable, ex);
            return new ContentResult { Content = error.JsonResponse(), StatusCode = (int?)error.StatusCode };
        }
    }

    /// <summary>
    /// Возвращает страны, у которых есть соответствующий индикатор
    /// </summary>
    /// <param name="indicatorId">ID индикатора</param>
    /// <param name="page">Страница</param>
    /// <param name="perPage">Количество записей на странице</param>
    /// <param name="countryNamePart">Часть названия стран, которые будут выведены</param>
    /// <remarks>
    /// Отсчет страниц начинается с 1. Т.е. при (page = 1, perPage = 50) вернутся первые 50 записей
    /// 
    /// Не больше 100 записей на страницу
    /// </remarks>
    /// <response code = "200"> Успешно </response> 
    /// <response code = "204"> Пустой ответ (необходимая информация отсутствует в БД)</response> 
    /// <response code = "503"> Ошибка подключения к базе данных  </response> 
    [Route("countriesByIndicator")]
    [HttpGet]
    public async Task<ActionResult<ChartResponse<PaginationMetadata, CountryViewModel>>> CountriesByIndicatorAsync
        (string indicatorId, int page = 1, int perPage = 50, string? countryNamePart = "")
    {
        try
        {
            CheckPageRangeThrowable(page, perPage);
            countryNamePart ??= "";
            var dbResult = await databaseWorker.GetCountriesThatHaveIndicatorAsync(indicatorId, page, perPage, countryNamePart);
            if (dbResult.Count == 0) return NoContent();

            var responseMetadata = new PaginationMetadata(await databaseWorker.GetCountriesThatHaveIndicatorCountAsync(indicatorId), page, perPage);
            var responseBody = dbResult
                .Select(x => new CountryViewModel(x.CountryId, x.CountryName))
                .ToList();
            var answer = new ChartResponse<PaginationMetadata, CountryViewModel>(responseMetadata, responseBody);

            return Ok(answer);
        }
        catch (Exception ex)
        {
            var error = new ApiError(HttpStatusCode.ServiceUnavailable, ex);
            return new ContentResult { Content = error.JsonResponse(), StatusCode = (int?)error.StatusCode };
        }
    }

    /// <summary>
    /// Возвращает индикаторы, у которых есть соответствующая страна
    /// </summary>
    /// <param name="countryId">ID страны</param>
    /// <param name="page">Страница</param>
    /// <param name="perPage">Количество записей на странице</param>
    /// <param name="indicatorNamePart">Часть названия индикаторов, которые будут выведены</param>
    /// <remarks>
    /// Отсчет страниц начинается с 1. Т.е. при (page = 1, perPage = 50) вернутся первые 50 записей
    /// 
    /// Не больше 100 записей на страницу
    /// </remarks>
    /// <response code = "200"> Успешно </response> 
    /// <response code = "204"> Пустой ответ (необходимая информация отсутствует в БД)</response> 
    /// <response code = "503"> Ошибка подключения к базе данных  </response> 
    [Route("indicatorsByCountry")]
    [HttpGet]
    public async Task<ActionResult<ChartResponse<PaginationMetadata, IndicatorViewModel>>> IndicatorsByCountryAsync(string countryId, int page = 1, int perPage = 50, string? indicatorNamePart = "")
    {
        try
        {
            CheckPageRangeThrowable(page, perPage);
            indicatorNamePart ??= "";
            var dbResult = await databaseWorker.GetIndicatorsThatHaveCountryAsync(countryId, page, perPage, indicatorNamePart);
            if (dbResult.Count == 0) return NoContent();

            var responseMetadata = new PaginationMetadata(await databaseWorker.GetIndicatorsThatHaveCountryCountAsync(countryId), page, perPage);
            var responseBody = dbResult
                .Select(x => new IndicatorViewModel(x.IndicatorId, x.IndicatorName))
                .ToList();
            var answer = new ChartResponse<PaginationMetadata, IndicatorViewModel>(responseMetadata, responseBody);

            return Ok(answer);
        }
        catch (Exception ex)
        {
            var error = new ApiError(HttpStatusCode.ServiceUnavailable, ex);
            return new ContentResult { Content = error.JsonResponse(), StatusCode = (int?)error.StatusCode };
        }
    }

    /// <summary>
    /// Возвращает информацию по указанным странам для выбранного индикатора
    /// </summary>
    /// <param name="countriesId">Список идентификаторов стран</param>
    /// <param name="indicatorId">Индикатор по которому фильтруем страны</param>
    /// <remarks>
    /// Пример запроса: 
    /// ``` 
    /// https://localhost:7280/api/infoByCountry?countriesId=AFE;AFW&amp;indicatorId=SP.POP.TOTL
    /// ```
    /// 
    /// Замечание: в качестве символа-разделителя используется ";"
    /// </remarks>
    /// <response code = "200"> Успешно </response> 
    /// <response code = "204"> Пустой ответ (необходимая информация отсутствует в БД)</response> 
    /// <response code = "503"> Ошибка подключения к базе данных  </response> 
    [Route("infoByIndicator")]
    [HttpGet]
    public async Task<ActionResult<ChartResponse<InfoByIndicatorMetadata, InfoByIndicatorViewModel>>> InfoByIndicatorAsync(string countriesId, string indicatorId)
    {
        try
        {
            const string delim = ";";
            const int mockPage = 1;

            var splittedCountriesId = new List<string>(countriesId.Split(delim));
            var dbResponseTotalRowsCount = await databaseWorker.GetInfoByIndicatorCountAsync(splittedCountriesId, indicatorId);
            var dbResult = await databaseWorker.GetInfoByIndicatorAsync(splittedCountriesId, indicatorId, mockPage, dbResponseTotalRowsCount);
            if (dbResult.Count == 0) return NoContent();

            //Building information with the number of response rows for the specified indicatorid
            var responseMetadata = new InfoByIndicatorMetadata(dbResult[0].Indicator.IndicatorId, dbResult[0].Indicator.IndicatorName, dbResponseTotalRowsCount);

            //Getting groups by country with values by years and full country name
            var countryGroups = dbResult.GroupBy(x => x.CountryId)
                .Select(x => new
                {
                    CountryId = x.First().Country.CountryId,
                    CountryName = x.First().Country.CountryName,
                    Values = x.Select(k => new YearValue { Year = k.Year, Value = k.Value })
                });

            //Building response with necessary fields
            var responseBody = countryGroups
                .Select(x => new InfoByIndicatorViewModel(x.CountryId, x.CountryName, x.Values.ToList()))
                .ToList();

            var answer = new ChartResponse<InfoByIndicatorMetadata, InfoByIndicatorViewModel>(responseMetadata, responseBody);

            return Ok(answer);
        }
        catch (Exception ex)
        {
            var error = new ApiError(HttpStatusCode.ServiceUnavailable, ex);
            return new ContentResult { Content = error.JsonResponse(), StatusCode = (int?)error.StatusCode };
        }
    }

    /// <summary>
    /// Возвращает информацию по указанным индикаторам для выбранной страны
    /// </summary>
    /// <param name="indicatorsId">Список идентификаторов индикаторов</param>
    /// <param name="countryId">Страна по которому фильтруем индикаторы</param>
    /// <remarks>
    /// Пример запроса: 
    /// ``` 
    /// https://localhost:7280/api/infoByIndicator?indicatorsId=SP.POP.TOTL;DP.DOD.DECD.CR.BC.CD&amp;countryId=AFE
    /// ```
    /// 
    /// Замечание: в качестве символа-разделителя используется ";"
    /// </remarks>
    /// <response code = "200"> Успешно </response> 
    /// <response code = "204"> Пустой ответ (необходимая информация отсутствует в БД)</response> 
    /// <response code = "503"> Ошибка подключения к базе данных  </response> 
    [Route("infoByCountry")]
    [HttpGet]
    public async Task<ActionResult<ChartResponse<InfoByCountryMetaData, InfoByCountryViewModel>>> InfoByCountryAsync(string indicatorsId, string countryId)
    {
        try
        {
            const string delim = ";";
            const int mockPage = 1;

            var splittedIndicatorsId = new List<string>(indicatorsId.Split(delim));
            var dbResponseTotalRowsCount = await databaseWorker.GetInfoByCountryCountAsync(splittedIndicatorsId, countryId);
            var dbResult = await databaseWorker.GetInfoByCountryAsync(splittedIndicatorsId, countryId, mockPage, dbResponseTotalRowsCount);
            if (dbResult.Count == 0) return NoContent();

            //Building information with the number of response rows for the specified country
            var responseMetadata = new InfoByCountryMetaData(dbResult[0].Country.CountryId, dbResult[0].Country.CountryName, dbResponseTotalRowsCount);

            //Getting groups by indicator with values by years and full indicator name
            var indicatorGroups = dbResult.GroupBy(x => x.IndicatorId)
                .Select(x => new
                {
                    IndicatorId = x.First().Indicator.IndicatorId,
                    IndicatorName = x.First().Indicator.IndicatorName,
                    Values = x.Select(k => new YearValue { Year = k.Year, Value = k.Value })
                });
            //Building response with necessary fields 
            var responseBody = indicatorGroups
                .Select(x => new InfoByCountryViewModel(x.IndicatorId, x.IndicatorName, x.Values.ToList()))
                .ToList();

            var answer = new ChartResponse<InfoByCountryMetaData, InfoByCountryViewModel>(responseMetadata, responseBody);

            return Ok(answer);
        }
        catch (Exception ex)
        {   
            var error = new ApiError(HttpStatusCode.ServiceUnavailable, ex);
            return new ContentResult { Content = error.JsonResponse(), StatusCode = (int?)error.StatusCode };
        }
    }
    
    private void CheckPageRangeThrowable(int page, int perPage)
    {
        if (page <= 0) throw new ArgumentException("Введены некорректные значения для выбора страницы (значение должно быть больше 0)");
        if (perPage <= 0 || perPage > perPageMax) throw new ArgumentException("Некорретные значения для количества записей на страницу (меньше 1 или больше максимального количества)");
    }
}