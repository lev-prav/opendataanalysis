using Microsoft.AspNetCore.Mvc;

namespace OpenDataAnalysis.Controllers;

[ApiExplorerSettings(IgnoreApi = true)]
[Route("")]
public class DrawController : Controller
{
    [Route("chart")]
    [Route("countries")]
    [Route("indicators")]
    public ActionResult Index()
    {
        return File("index.html", "text/html");;
    }
}