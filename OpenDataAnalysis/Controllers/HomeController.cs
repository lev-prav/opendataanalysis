﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using OpenDataAnalysis.Models;

namespace OpenDataAnalysis.Controllers;

[ApiExplorerSettings(IgnoreApi = true)]
[Route("Home")]
public class HomeController : Controller
{
    private readonly ILogger<HomeController> logger;

    public HomeController(ILogger<HomeController> logger)
    {
        this.logger = logger;
    }

    [Route("")]
    [Route("Index")]
    public IActionResult Index()
    {
        return View();
    }

    [Route("Privacy")]
    public IActionResult Privacy()
    {
        return View();
    }

    [Route("Chart")]
    public IActionResult Chart()
    {
        return View();
    }

    [Route("Error")]
    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}