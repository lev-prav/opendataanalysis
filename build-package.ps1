﻿Push-Location $PSScriptRoot

Echo "Building frontend..."
cd .\frontend
& npm install
$env:NODE_ENV="production" ; & npm run build
cd ..

Echo ""
Echo "Building web application..."
& dotnet publish -c Release -p:EnvironmentName=Production -r win-x64 --no-self-contained -o ".\temp"

Echo ""
Echo "Compressing the package..."
$packageName = ".\lions-" + (Get-Date).ToString("yyyy-MM-dd-HH-mm-ss") + ".zip";

Get-Item ".\temp\appSettings__*.json" | Remove-Item
Compress-Archive -Path .\temp\* -DestinationPath $packageName

Echo ""
Echo "Cleaning up..."
Remove-Item ".\temp" -Recurse -Force

Echo ""
Echo ""
Echo "The package name is $packageName"

Pop-Location