using FillDBApp.DataModels;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace FillDBApp;

public class Filler
{
    private const int PerIteration = 1006;
    DataSourceAPI _dataSource;
    DbHandler _dbHandler;
    private readonly ILogger<Filler> _logger;

    public Filler(ILogger<Filler> logger, DataSourceAPI dataSource, DbHandler dbHandler)
    {
        _logger = logger;
        _dataSource = dataSource;
        _dbHandler = dbHandler;
    }

    public async Task FillCountriesAsync(string CountriesTable)
    {
        HashSet<Country> countriesInDatabase = new HashSet<Country>(_dbHandler.SelectAllFrom<Country>(CountriesTable));

        //Converting all countries from the dataSource to HashSet
        var sourceCountriesSet = new HashSet<Country>();
        await foreach (var countries in _dataSource.GetCountries(PerIteration))
        {
            sourceCountriesSet.UnionWith(countries);
        }

        //Getting only unuqie countries from the source to add them to our database
        var uniqueSourceCountries = sourceCountriesSet.ExceptBy(countriesInDatabase.Select(x => x.Code), x => x.Code);
        if (uniqueSourceCountries.Count() > 0)
            await _dbHandler.PutInBaseAsync(uniqueSourceCountries.ToList(), CountriesTable);
        else
            _logger.LogInformation($"{CountriesTable}: no countries to add");

        //Getting only unique countries from our database
        //Unique values can be countries that partially match the new countries from source and therefore they need to be updated,  
        //or they can be countries that are not at all in the source, then they need to be deleted
        var uniqueDatabaseCountries = countriesInDatabase.Except(sourceCountriesSet);

        var countriesToUpdate = new List<Country>();
        foreach (var uniqueDatabaseCountry in uniqueDatabaseCountries)
        {
            countriesToUpdate.AddRange(sourceCountriesSet.Where(x => x.Code == uniqueDatabaseCountry.Code));
        }
        await _dbHandler.UpdateCountriesAsync(countriesToUpdate.ToList(), CountriesTable);

        var countriesToDelete = new HashSet<Country>(uniqueDatabaseCountries.ExceptBy(countriesToUpdate.Select(x => x.Code), c => c.Code));
        await _dbHandler.DeleteCountriesFromBaseAsync(countriesToDelete.ToList(), CountriesTable);

        if (countriesToUpdate.Count() <= 0)
            _logger.LogInformation($"{CountriesTable}: no countries to update");
        if (countriesToDelete.Count <= 0)
            _logger.LogInformation($"{CountriesTable}: no countries to delete");
    }

    public async Task FillIndicatorsAsync(string IndicatorsTable)
    {
        HashSet<Indicator> indicatorsInDatabase = new HashSet<Indicator>(_dbHandler.SelectAllFrom<Indicator>(IndicatorsTable));

        //Converting all indicators from the dataSource to HashSet
        var sourceIndicatorsSet = new HashSet<Indicator>();
        await foreach (var indicators in _dataSource.GetIndicators(PerIteration))
        {
            sourceIndicatorsSet.UnionWith(indicators);
        }

        //Setting current date for all values
        sourceIndicatorsSet.ToList().ForEach(x => x.UpdatedAt = DateTime.Now);

        //Getting only unuqie indicators from the source to add them to our database
        var uniqueSourceIndicators = sourceIndicatorsSet.ExceptBy(indicatorsInDatabase.Select(x => x.IndicatorId), x => x.IndicatorId);
        if (uniqueSourceIndicators.Count() > 0)
            await _dbHandler.PutInBaseAsync(uniqueSourceIndicators.ToList(), IndicatorsTable);
        else
            _logger.LogInformation($"{IndicatorsTable}: no indicators to add");

        //Getting only unique indicators from our database
        //Unique values can be indicators that partially match the new indicators from source and therefore they need to be updated,  
        //or they can be indicators that are not at all in the source, then they need to be deleted
        var uniqueDatabaseIndicators = indicatorsInDatabase.Except(sourceIndicatorsSet);

        List<Indicator> indicatorsToUpdate = new List<Indicator>();
        foreach (var uniqueDatabaseIndicator in uniqueDatabaseIndicators)
        {
            indicatorsToUpdate.AddRange(sourceIndicatorsSet.Where(x => x.IndicatorId == uniqueDatabaseIndicator.IndicatorId));
        }
        await _dbHandler.UpdateIndicatorsAsync(indicatorsToUpdate.ToList(), IndicatorsTable);

        HashSet<Indicator> indicatorsToDelete = new HashSet<Indicator>(uniqueDatabaseIndicators.ExceptBy(indicatorsToUpdate.Select(x => x.IndicatorId), c => c.IndicatorId));
        await _dbHandler.DeleteIndicatorsFromBaseAsync(indicatorsToDelete.ToList(), IndicatorsTable);

        if (indicatorsToUpdate.Count() <= 0)
            _logger.LogInformation($"{IndicatorsTable}: no indicators to update");
        if (indicatorsToDelete.Count <= 0)
            _logger.LogInformation($"{IndicatorsTable}: no indicators to delete");
    }

    public async Task FillCountryIndicatorAsync(string CountriesTable, string IndicatorsTable, string CountryIndicatorsTable)
    {
        var indicatorsIdList = _dbHandler.SelectAllFrom<Indicator>(IndicatorsTable).Select(x => x.IndicatorId);
        var countriesHashSet = _dbHandler.SelectAllFrom<Country>(CountriesTable).Select(x => x.Code).ToHashSet();
        var indicatorsHashSet = indicatorsIdList.ToHashSet();
        foreach (var indicatorId in indicatorsIdList)
        {
            var currentSet = new HashSet<CountryIndicator>();

            await foreach (var responseData in _dataSource.GetCountryIndicator(indicatorId, 1000))
            {
                currentSet.UnionWith(responseData);
            }

            var verifiedSet = currentSet.Where(x => countriesHashSet.Contains(x.CountryId) && indicatorsHashSet.Contains(x.IndicatorId)).ToList();

            await _dbHandler.UpdateCountryIndicatorsAsync(verifiedSet.ToList(), indicatorId, CountryIndicatorsTable);
        }
    }
}