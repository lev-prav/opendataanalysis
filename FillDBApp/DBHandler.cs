using System.ComponentModel;
using System.Data;
using FillDBApp.DataModels;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Logging;

namespace FillDBApp;

public class DbHandler
{
    private readonly string _connectionString;
    private readonly ILogger<DbHandler> _logger;

    public DbHandler(ILogger<DbHandler> logger, string connectionString)
    {
        _logger = logger;
        _connectionString = connectionString;
        logger.LogInformation($@"Connected to {connectionString}");
    }

    //BUG: dont use it with CountryIndicator table as T, need changes 
    public List<T> SelectAllFrom<T>(string tableName) where T : new()
    {
        List<T> table = new List<T>();

        PropertyDescriptorCollection props =
            TypeDescriptor.GetProperties(typeof(T));

        using SqlConnection connection = new SqlConnection(_connectionString);
        connection.Open();
        try
        {
            using SqlCommand command = new SqlCommand(
                $"SELECT * FROM {tableName}", connection);
            var reader = command.ExecuteReader();
            while (reader.Read())
            {
                T obj = new T();
                for (int i = 0; i < props.Count; i++)
                {
                    props[i].SetValue(obj, reader[i]);
                }
                table.Add(obj);
            }
        }
        catch (Exception e)
        {
            //TODO: make a correct exception handle 
            _logger.LogError(e, $@"SelectAllFrom table {tableName}");
        }

        //TODO: implement per string reading, may be in enumerable
        return table;
    }

    public async Task PutInBaseAsync<T>(List<T> dataList, string tableName)
    {
        if (IsIncorrectData(dataList)) return;
        var dataTable = ToDataTable(dataList);
        using SqlConnection connection = new SqlConnection(_connectionString);
        using SqlBulkCopy bulkCopy = new SqlBulkCopy(_connectionString, SqlBulkCopyOptions.CheckConstraints);

        connection.Open();
        bulkCopy.DestinationTableName = tableName;
        try
        {
            await bulkCopy.WriteToServerAsync(dataTable);
            _logger.LogInformation($"{tableName}: writed {dataList.Count} lines");
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, $@"DBHandler.PutInBase({typeof(T)}): ");
        }
    }

    private static DataTable ToDataTable<T>(IList<T> data)
    {
        PropertyDescriptorCollection props =
            TypeDescriptor.GetProperties(typeof(T));
        DataTable table = new DataTable();
        for (int i = 0; i < props.Count; i++)
        {
            PropertyDescriptor prop = props[i];
            table.Columns.Add(prop.Name, prop.PropertyType);
        }
        object[] values = new object[props.Count];
        foreach (T item in data)
        {
            for (int i = 0; i < values.Length; i++)
            {
                values[i] = props[i].GetValue(item);
            }
            table.Rows.Add(values);
        }
        return table;
    }

    public async Task UpdateCountriesAsync(List<Country> actualCountries, string tableName)
    {
        if (IsIncorrectData(actualCountries)) return;
        using (SqlConnection connection = new SqlConnection(_connectionString))
        {
            connection.Open();
            foreach (var country in actualCountries)
            {
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText =
                        $"UPDATE {tableName} " +
                         "SET CountryName = @Name " +
                         "WHERE CountryId = @Code";

                    command.Parameters.AddWithValue("@Name", country.Name);
                    command.Parameters.AddWithValue("@Code", country.Code);

                    await command.ExecuteNonQueryAsync();
                }
            }
            connection.Close();
            _logger.LogInformation($"{tableName}: updated {actualCountries.Count} lines");
        }
    }

    public async Task UpdateIndicatorsAsync(List<Indicator> actualIndicators, string tableName)
    {
        if (IsIncorrectData(actualIndicators)) return;
        using (SqlConnection connection = new SqlConnection(_connectionString))
        {
            connection.Open();
            foreach (var indicator in actualIndicators)
            {
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText =
                        $"UPDATE {tableName} " +
                         "SET IndicatorName = @Description, Updated = @DateTimeNow " +
                         "WHERE IndicatorId = @IndicatorId";

                    command.Parameters.AddWithValue("@Description", indicator.Description);
                    command.Parameters.AddWithValue("@DateTimeNow", DateTime.Now);
                    command.Parameters.AddWithValue("@IndicatorId", indicator.IndicatorId);

                    await command.ExecuteNonQueryAsync();
                }
            }
            connection.Close();
            _logger.LogInformation($"{tableName}: updated {actualIndicators.Count} lines");
        }
    }

    public async Task UpdateCountryIndicatorsAsync(List<CountryIndicator> actualCountryIndicators, string indicatorId, string tableName)
    {
        if (actualCountryIndicators == null || actualCountryIndicators.Count <= 0)
        {
            _logger.LogInformation(
                        $"CountryIndicators: {indicatorId} not valid");
            return;
        }
        using (SqlConnection connection = new SqlConnection(_connectionString))
        {
            connection.Open();
            using (SqlCommand command = connection.CreateCommand())
            {
                command.CommandText =
                    $"DELETE FROM {tableName} " +
                     "WHERE IndicatorId = @IndicatorId";

                command.Parameters.AddWithValue("@IndicatorId", indicatorId);

                await command.ExecuteNonQueryAsync();

                _logger.LogInformation(
                    $"CountryIndicators: Try to write {indicatorId}");
                await PutInBaseAsync(actualCountryIndicators, tableName);
            }
            connection.Close();
        }
    }

    public async Task DeleteCountriesFromBaseAsync(List<Country> countriesToDelete, string tableName)
    {
        if (IsIncorrectData(countriesToDelete)) return;
        using (SqlConnection connection = new SqlConnection(_connectionString))
        {
            connection.Open();
            foreach (var country in countriesToDelete)
            {
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText =
                        $"DELETE FROM {tableName} " +
                         "WHERE CountryId = @Code";

                    command.Parameters.AddWithValue("@Code", country.Code);

                    await command.ExecuteNonQueryAsync();
                }
            }
            connection.Close();
            _logger.LogInformation($"{tableName}: deleted {countriesToDelete.Count} lines");
        }
    }

    public async Task DeleteIndicatorsFromBaseAsync(List<Indicator> indicatorsToDelete, string tableName)
    {
        if (IsIncorrectData(indicatorsToDelete)) return;
        using (SqlConnection connection = new SqlConnection(_connectionString))
        {
            connection.Open();
            foreach (var indicator in indicatorsToDelete)
            {
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText =
                        $"DELETE FROM {tableName} " +
                         "WHERE IndicatorId = @IndicatorId";

                    command.Parameters.AddWithValue("@IndicatorId", indicator.IndicatorId);

                    await command.ExecuteNonQueryAsync();
                }
            }
            connection.Close();
            _logger.LogInformation($"{tableName}: deleted {indicatorsToDelete.Count} lines");
        }
    }

    private bool IsIncorrectData<T>(List<T> data) => data == null || data.Count <= 0;
}