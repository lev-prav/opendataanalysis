using Newtonsoft.Json;

namespace FillDBApp.DataModels;

public class CountryIndicator : IEquatable<CountryIndicator>
{
    [JsonProperty("countryiso3code")]
    public string CountryId { get; set; }
    [JsonProperty("indicator")]
    public string IndicatorId { get; set; }
    public int Year { get; set; }
    public decimal Value { get; set; }

    public bool Equals(CountryIndicator? other)
    {
        if (ReferenceEquals(null, other)) return false;
        if (ReferenceEquals(this, other)) return true;
        return CountryId == other.CountryId && IndicatorId == other.IndicatorId && Year == other.Year;
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(CountryId, IndicatorId, Year);
    }
}

public class CountryIndicatorJson : IEquatable<CountryIndicatorJson>
{
    [JsonProperty("date")]
    public string Year { get; set; }
    [JsonProperty("value")]
    public decimal? Value { get; set; }
    [JsonProperty("country")]
    public CountryJson Country { get; set; }

    public bool Equals(CountryIndicatorJson? other)
    {
        if (ReferenceEquals(null, other)) return false;
        if (ReferenceEquals(this, other)) return true;
        return Year == other.Year && Country.Equals(other.Country);
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(Year, Country);
    }

    public class CountryJson : IEquatable<CountryJson>
    {
        public string Id;
        public string Value;

        public bool Equals(CountryJson? other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Id == other.Id && Value == other.Value;
        }
        public override int GetHashCode()
        {
            return HashCode.Combine(Id, Value);
        }
    }
}