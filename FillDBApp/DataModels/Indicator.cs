﻿using Newtonsoft.Json;

namespace FillDBApp.DataModels
{
    public class Indicator : IEquatable<Indicator>
    {
        [JsonProperty("id")]
        public string IndicatorId { get; set; }
        [JsonProperty("name")]
        public string Description { get; set; }
        public DateTime UpdatedAt { get; set; }

        public bool Equals(Indicator? other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return IndicatorId == other.IndicatorId && Description == other.Description;
        }
        public override int GetHashCode()
        {
            return HashCode.Combine(IndicatorId, Description);
        }

        public override string ToString()
        {
            return $"Indicator: id: {IndicatorId}, name:{Description}";
        }
    }
}
