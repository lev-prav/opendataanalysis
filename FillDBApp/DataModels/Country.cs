﻿using Newtonsoft.Json;

namespace FillDBApp.DataModels
{
    public class Country : IEquatable<Country>
    {
        [JsonProperty("id")]
        public string Code { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }

        public bool Equals(Country? other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Code == other.Code && Name == other.Name;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Code, Name);
        }
    }
}
