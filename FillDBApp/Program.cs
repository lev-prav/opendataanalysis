﻿// See https://aka.ms/new-console-template for more information

using System.Diagnostics;
using FillDBApp;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

IHostBuilder CreateHostBuilder() =>
    Host.CreateDefaultBuilder()
        .ConfigureAppConfiguration((hostingContext, config) =>
        {
            config.AddConfiguration(
                new ConfigurationBuilder().
                    AddJsonFile("appsettings.json")
                    .Build());
        })
        .ConfigureLogging(logging =>
        {
            logging.ClearProviders();
            logging.AddConsole();
            logging.AddDebug();
        })
        .ConfigureServices((hostBuilderContext, services) =>
        {
            services.AddSingleton(
                serviceProvider => new DbHandler(
                    serviceProvider.GetRequiredService<ILogger<DbHandler>>()!,
                    hostBuilderContext.Configuration.GetConnectionString("Connection")!
                    )
                );
            services.AddSingleton<DataSourceAPI>();
            services.AddSingleton<Filler>();
        });


using IHost host = CreateHostBuilder().Build();
var filler = host.Services.GetService<Filler>()!;

Stopwatch stopwatch = new Stopwatch();
stopwatch.Start();

const string CountriesTable = "Countries";
const string IndicatorsTable = "Indicators";
const string CountryIndicatorsTable = "CountryIndicators";

var countriesTask = filler.FillCountriesAsync(CountriesTable);
var indicatorsTask = filler.FillIndicatorsAsync(IndicatorsTable);
Task.WaitAll(countriesTask, indicatorsTask);
await filler.FillCountryIndicatorAsync(CountriesTable, IndicatorsTable, CountryIndicatorsTable);

stopwatch.Stop();
Console.WriteLine($"Time passed: {stopwatch.ElapsedMilliseconds} milliseconds");