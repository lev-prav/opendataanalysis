using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using FillDBApp.DataModels;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

public class DataSourceAPI
{
    static readonly HttpClient Client = new HttpClient();
    private readonly ILogger<DataSourceAPI> _logger;
    const string ApiBaseUrl = "http://api.worldbank.org/v2";
    const string Format = "json";

    public DataSourceAPI(ILogger<DataSourceAPI> logger)
    {
        _logger = logger;
    }

    public async IAsyncEnumerable<HashSet<Country>> GetCountries(int perPage)
    {
        string urlTail = "country/all";
        await foreach (var countries in GetDeserializedResponse<Country>(urlTail, perPage))
        {
            yield return countries;
        }
    }
    public async IAsyncEnumerable<HashSet<Country>> GetCountriesByIndicator(string indicatorName, int perPage = 50)
    {
        string urlTail = $"country/all/indicator/{indicatorName}";
        await foreach (var countries in GetDeserializedResponse<Country>(urlTail, perPage))
        {
            yield return countries;
        }
    }

    public async IAsyncEnumerable<HashSet<Indicator>> GetIndicators(int perPage)
    {
        string urlTail = "indicator/all";
        await foreach (var indicatorsSet in GetDeserializedResponse<Indicator>(urlTail, perPage))
        {
            yield return indicatorsSet;
        }
    }

    public async IAsyncEnumerable<HashSet<CountryIndicator>> GetCountryIndicator(string indicatorId, int perPage)
    {
        string urlTail = $"country/all/indicator/{indicatorId}";
        await foreach (var countryIndicatorJsonSet in GetDeserializedResponse<CountryIndicatorJson>(urlTail, perPage))
        {
            yield return countryIndicatorJsonSet
                .Where(respObject => respObject.Value != null)
                .Select(respObject =>
                {
                    int year = 0;
                    int.TryParse(respObject.Year[..4], out year);
                    return new CountryIndicator()
                    {
                        CountryId = respObject.Country.Id,
                        IndicatorId = indicatorId,
                        Value = respObject.Value ?? 0,
                        Year = year
                    };
                })
                .ToHashSet();
        }
    }

    private async IAsyncEnumerable<HashSet<T>> GetDeserializedResponse<T>(string urlTail, int perPage)
    {
        var metaInfo = await GetMetaData(urlTail, perPage);

        for (int page = 1; page <= metaInfo.pages; page++)
        {
            var response = await GetResponse(urlTail, page: page, perPage: perPage);
            if (response is null) continue;
            string responseData;
            try
            {
                responseData = GetResponseData(response);
            }
            catch (Exception)
            {
                continue;
            }
            yield return DeserializeJson<T>(responseData);
        }
    }

    private string GetResponseData(string response)
    {
        return JsonConvert.DeserializeObject<JArray>(response)![1].ToString();

    }

    private async Task<string?> GetResponse(string URL, int page = 1, int perPage = 50)
    {
        var url = $"{ApiBaseUrl}" +
                  $"/{URL}" +
                  $"?format={Format}&page={page}&per_page={perPage}";
        try
        {
            return await Client.GetStringAsync(url);
        }
        catch (HttpRequestException ex)
        {
            //Console.WriteLine(e.Message);
            _logger.LogError(ex, $@"URL: {url};\n");
        }
        //TODO add wrong response cod here
        return null;
    }

    private HashSet<T> DeserializeJson<T>(string dataJson)
    {
        return JsonConvert.DeserializeObject<HashSet<T>>(dataJson)!;
    }

    private async Task<MetaData> GetMetaData(string urlTail, int perPage)
    {
        var response = await GetResponse(urlTail, perPage: perPage);
        if (response is null)
        {
            return new MetaData()
            {
                page = 1,
                pages = 0,
                per_page = 0,
                total = 0
            };
        }
        try
        {
            var json = JsonConvert.DeserializeObject<JArray>(response)![0];
            return json.ToObject<MetaData>()!;
        }
        catch
        {
            return new MetaData()
            {
                page = 1,
                pages = 0,
                per_page = 0,
                total = 0
            };
        }
    }

    public class MetaData
    {
        public int page { get; set; }
        public int pages { get; set; }
        public int per_page { get; set; }
        public int total { get; set; }
    }
}