import axios, {AxiosResponse} from "axios";

const fetchContent = async (url: string, params: object | undefined) => {
    return axios.get(url, params).then((response) => {
        return response.data;
    }).catch((err) => {
        console.log(err);
    });
}

const createURL = (path: string, params: object): string => {
    let res = [];
    for (let prop in params) {
        // @ts-ignore
        res.push(`${prop}=${params[prop]}`);
    }
    let query = res.join('&');
    return `${path}?${query}`;
}

const parseResponse = (response: Array<any>) => {
    return {
        meta: response[0],
        content: response[1]
    }
}

export {fetchContent, createURL, parseResponse};