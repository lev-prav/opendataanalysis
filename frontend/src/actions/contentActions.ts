const capitalize = (line: string): string => {
    if (line.length === 0){
        return line;
    }
    return line[0].toUpperCase() + line.slice(1);
}

const getYearRange = (data: Array<any>) => {
    let min: number = Infinity;
    let max: number = -Infinity;

    for (let dataset of data) {
        for (let value of dataset.values) {
            const year: number = +value.year;
            if (year < min) {
                min = year;
            }
            if (year > max) {
                max = year;
            }
        }
    }
    return {
        'min': min,
        'max': max
    };
}

const parseDataset = (minYear: number, maxYear: number, values: any[]): any[] => {
    let normalizedValues = new Array(maxYear - minYear + 1).fill(null);
    values.forEach((value) => {
        const year = +value.year;
        if (year >= minYear && year <= maxYear){
            const idx = year - minYear;
            normalizedValues[idx] = +value.value;
        }
    });
    return normalizedValues;
}

export { capitalize, getYearRange, parseDataset };