import {useEffect, useState} from 'react';
import Box from '@mui/material/Box';
import Slider from '@mui/material/Slider';
import {Typography} from "@mui/material";


export default function YearSlider(props: any) {
    const [value, setValue] = useState<number[]>([+props.minYear, +props.maxYear]);

    useEffect((): void => {
        setValue([+props.minYear, +props.maxYear]);
    }, [props.minYear, props.maxYear]);

    const handleChange = (event: Event, newValue: number | number[]) => {
        setValue(newValue as number[]);
        props.onYearsChanged(newValue as number[]);
    };

    return (
        <div style={{
            width: '100%'
        }}>
            <Typography gutterBottom>Year range:</Typography>
            <Slider
                getAriaLabel={() => 'Year range'}
                value={value}
                min={+props.minYear}
                max={+props.maxYear}
                onChange={handleChange}
                valueLabelDisplay="auto"
            />
        </div>
    );
}