import React from 'react';
import {
    ArcElement,
    BarElement,
    CategoryScale,
    Chart as ChartJS,
    Legend,
    LinearScale,
    LineElement,
    PointElement,
    Title,
    Tooltip,
} from 'chart.js';
import {Bar, Line, Pie} from 'react-chartjs-2';
import {ChartMode} from "../pages/ChartPage";
import {parseDataset} from "../../actions/contentActions";
import TableChart from "../TableChart";

ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    BarElement,
    ArcElement,
    Title,
    Tooltip,
    Legend
);

interface Dataset {
    labels: string[],
    datasets: any[]
}


const ChartTab = (props: any) => {
    const options = {
        responsive: true,
        plugins: {
            legend: {
                position: 'top' as const,
            },
            title: {
                display: true,
                text: `Chart for ${props.chartTitle}`,
            },
        },
        maintainAspectRatio: false
    };

    const defineLabels = (): string[] => {
        let labels = [];
        for (let year = props.minYear; year <= props.maxYear; year++) {
            labels.push(`${year}`);
        }
        return labels;
    }

    const defineData = (labels: string[]) => {
        const datasets = props.content;
        let data: Dataset = {
            labels: labels,
            datasets: []
        }

        for (let dataset of datasets) {
            const title = dataset.indicatorName ?? dataset.countryName;
            let current = {
                "label": title,
                "data": parseDataset(+props.minYear, +props.maxYear, dataset.values),
                "borderColor": dataset.borderColor ?? 'rgb(0, 0, 255)',
                "backgroundColor": dataset.backgroundColor ?? `rgba(0, 0, 255, 0.5)`
            };
            data.datasets.push(current);
        }
        return data;
    }

    const data = defineData(defineLabels());

    const defineChart = (mode: ChartMode) => {
        switch(mode){
            case ChartMode.Line:
                return <Line style={{
                    maxHeight: '50vh'
                }} options={options} data={data} />;
            case ChartMode.Histogram:
                return <Bar style={{
                    maxHeight: '50vh'
                }} options={options} data={data} />;
            case ChartMode.Table:
                return <TableChart modificator={props.chartTitle} headers={data.labels} content={data.datasets}/>
            default:
                return null;
        }
    }

    return (
        <>
            { defineChart(props.chartMode) }
        </>
    );
}

export default ChartTab;