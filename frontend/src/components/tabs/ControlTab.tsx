import {Button, ButtonGroup, Grid, Paper, styled, Typography} from "@mui/material";
import {ChartMode} from "../pages/ChartPage";
import YearSlider from "../YearSlider";

const FocusedButton = styled(Button)({
    '&:focus': {
        backgroundColor: '#0069d9',
        color: '#FFFFFF'
    },
});


const ControlTab = (props: any) => {
    return (
        <Grid container spacing={2} alignItems={'center'} sx={{
            marginTop: '1rem',
            width: '100%'
        }}>
            <Grid item lg={6} md={12} sm={12} xs={12}>
                <ButtonGroup variant={'outlined'}>
                    <FocusedButton
                        onClick={
                            () => {
                                props.onChartModeChange(ChartMode.Line);
                            }
                        } > Line chart </FocusedButton>
                    <FocusedButton onClick={
                        () => {
                            props.onChartModeChange(ChartMode.Histogram);
                        }
                    }> Histogram </FocusedButton>
                    <FocusedButton onClick={
                        () => {
                            props.onChartModeChange(ChartMode.Table);
                        }
                    }> Table </FocusedButton>
                </ButtonGroup>
            </Grid>
            <Grid item lg={6} md={12} sm={12} xs={12}>
                <YearSlider minYear={props.minYear} maxYear={props.maxYear} onYearsChanged={props.onYearsChanged} />
            </Grid>
        </Grid>
    );
}

export default ControlTab;