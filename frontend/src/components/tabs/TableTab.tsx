import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import {CircularProgress, Link, TableFooter, TablePagination, TextField, Typography} from "@mui/material";
import React, {useEffect, useState} from "react";
import {server} from "../../index";
import useDebounce from "../../hooks/useDebounce";
import {fetchContent, createURL} from "../../actions/httpActions";
import {CurrentModificator} from "../pages/MainPage";


enum Mode {
    Countries = 'countries',
    Indicators = 'indicators'
}

const countriesHeaders: string[] = ['name'];
const indicatorsHeaders: string[] = ['description'];
const initialData: any = [];
const columnsCount: number = 4;


const TableTab = (props: any) => {
    const [data, setData] = useState<Array<any>>(initialData);
    const [page, setPage] = useState<number>(0);
    const [rowsPerPage, setRowsPerPage] = useState<number>(20);
    const [pages, setPages] = useState<number>(1);
    const [searchItem, setSearchItem] = useState<string>('');
    const [isLoading, setLoading] = useState<boolean>(false);
    const debouncedSearchItem = useDebounce(searchItem, 500);

    const defineHeadersContent = (): string[] => {
        if (props.mode !== Mode.Countries){
            return indicatorsHeaders;
        }
        return countriesHeaders;
    }

    const definePath = (): string => {
        let path: string;

        if (props.mode === Mode.Countries){
            path = `${server}api/country/`;
            return createURL(path,
                {
                    'page': `${page + 1}`,
                    'perPage': `${rowsPerPage}`,
                    'countryNamePart': `${debouncedSearchItem}`
                });
        }

        path = `${server}api/indicators/`;
        return createURL(path,
            {
                'page': `${page + 1}`,
                'perPage': `${rowsPerPage}`,
                'indicatorNamePart': `${debouncedSearchItem}`
            });
    }

    const defineParams = (id: string): CurrentModificator => {
        if (props.mode === Mode.Countries){
            return {
                mode: `${Mode.Indicators}`,
                id: id
            };
        }
        return {
            mode: `${Mode.Countries}`,
            id: id
        }
    }

    const loadData = (): void => {
        setLoading(true);
        fetchContent(definePath(), {})
            .then((content) => {
                setData(content.responseBody);
                setPages(+content.responseMetadata.total);
            })
            .catch((err) => {
                console.log(err);
            })
            .finally(() => {
                setLoading(false);
            });
    }

    const headers: string[] = defineHeadersContent();

    useEffect((): void => {
        loadData();
    }, [page, rowsPerPage]);

    useEffect((): void => {
        loadData();
        setPage(0);
    }, [debouncedSearchItem]);

    const handleChangePage = (
        event: React.MouseEvent<HTMLButtonElement> | null,
        newPage: number,
    ): void => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (
        event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
    ): void => {
        let newRowsPerPage = parseInt(event.target.value, 10);
        setPage(0);
        setRowsPerPage(newRowsPerPage);
    };

    const defineHeader = (): JSX.Element => {
        return (
            <TableRow>
                <TableCell> № </TableCell>
                {headers.map((item: string) => {
                    return <TableCell>
                        <TextField
                            onChange={(event) => {
                                setSearchItem(event.target.value);
                            }} label={`Search by ${item}:`}  variant={"standard"}/>
                    </TableCell>
                })}
                <TableCell> Link </TableCell>
                <TableCell> { props.mode === Mode.Countries ? "Indicators count" : "Countries count" } </TableCell>
            </TableRow>
        );
    }

    const defineIndex = (idx: number): number => {
        return (rowsPerPage * page) + idx + 1;
    }

    const defineData = () => {
        if (!data || data?.length === 0) {
            return (
                <TableRow>
                    <TableCell colSpan={columnsCount} align={'center'}>
                        <Typography> Nothing found </Typography>
                    </TableCell>
                </TableRow>
            );
        }
        return data.map((item: any, idx: number): any => {
            const count = item.indicatorsCount ?? item.countryCount ?? 0;
            return (
                <TableRow>
                    <TableCell> {defineIndex(idx)} </TableCell>
                    <TableCell> {item['countryName'] ?? item['indicatorName']} </TableCell>
                    <TableCell>
                        <Link
                            disabled={count === 0}
                            color={count !== 0 ? 'primary' : 'disabled'}
                            component={'button'}
                            onClick={() => {
                                props.handleCurrent(defineParams(item['countryId'] ?? item['indicatorId']));
                            }}
                            underline={'none'}>
                            { props.mode === Mode.Countries ? "Choose indicators" : "Choose countries" }
                        </Link>
                    </TableCell>
                    <TableCell>
                        { count }
                    </TableCell>
                </TableRow>
            );
        });
    }

    return (
        <>
        <TableContainer className={'table-container'} sx={{
            width: {
                xxs: "90%",
                xs: "90%",
                sm: "90%",
                md: "80%",
                lg: "50%",
                xl: "50%"
            },
            margin: 'auto',
            marginTop: '2rem'
        }} component={Paper}>
            <Table>
                <TableHead>
                    {defineHeader()}
                </TableHead>
                <TableBody>
                    { isLoading ? (
                        <TableRow>
                            <TableCell colSpan={columnsCount} align={'center'}>
                                <CircularProgress/>
                            </TableCell>
                        </TableRow>
                    ) : defineData() }
                </TableBody>
                <TableFooter>
                    <TableRow>
                        <TablePagination
                            rowsPerPageOptions={[10, 20, 50]}
                            colSpan={columnsCount}
                            count={pages}
                            rowsPerPage={rowsPerPage}
                            page={page}
                            SelectProps={{
                                inputProps: {
                                    'aria-label': 'rows per page',
                                },
                                native: true,
                            }}
                            onPageChange={handleChangePage}
                            onRowsPerPageChange={handleChangeRowsPerPage}
                        />
                    </TableRow>
                </TableFooter>
            </Table>
        </TableContainer>
        </>
    );
}

export default TableTab;
export { Mode };