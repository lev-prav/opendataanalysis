import {AppBar, Box, Container, createTheme, ThemeProvider, Toolbar, Typography} from "@mui/material";
import HomeOutlinedIcon from '@mui/icons-material/HomeOutlined';

interface Page {
    page: string,
    link: string
}

const darkTheme = createTheme({
    palette: {
        mode: 'dark',
        primary: {
            main: '#1976d2',
        },
    },
});


const MainNavbar = (props: any) => {
    const pages: Array<Page> = [
        {
            page: "Countries",
            link: "/countries/"
        },
        {
            page: "Indicators",
            link: "/indicators/"
        }
        ];

    return (
        <ThemeProvider theme={darkTheme}>
            <AppBar position="static" color="primary">
                <Container data-testid={'main-navbar'} maxWidth={'xl'} sx={{
                    display: 'flex',
                    justifyContent: 'flex-start',
                    alignItems: 'center'
                }}>
                    <Toolbar>
                        <Typography variant="h6" component="a" href={'/'} sx={{
                            color: 'white',
                            textDecoration: 'none',
                            marginRight: '1rem'
                        }}>
                            <HomeOutlinedIcon fontSize="large"/>
                        </Typography>
                        {
                            pages.map((page: Page) => {
                                return (
                                    <Typography variant="h6" component="a" href={page.link} sx={{
                                        color: 'white',
                                        textDecoration: 'none',
                                        marginRight: '1rem'
                                    }}>
                                        {page.page}
                                    </Typography>
                                );
                            })
                        }
                    </Toolbar>
                </Container>
            </AppBar>
        </ThemeProvider>
    );
}

export default MainNavbar;