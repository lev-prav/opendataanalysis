import {
    Dialog,
    DialogActions,
    DialogTitle,
    DialogContent,
    Button,
    TableFooter,
    TablePagination,
    Checkbox, TextField, CircularProgress, Typography
} from "@mui/material";
import React, {useEffect, useState} from "react";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableHead from "@mui/material/TableHead";
import TableBody from "@mui/material/TableBody";
import TableRow from "@mui/material/TableRow";
import TableContainer from "@mui/material/TableContainer";
import FormControlLabel from '@mui/material/FormControlLabel';
import TableCell from "@mui/material/TableCell";
import {Mode} from "../tabs/TableTab";
import {fetchContent, createURL} from "../../actions/httpActions";
import {server} from "../../index";
import {NavigateFunction, useNavigate} from "react-router";
import useDebounce from "../../hooks/useDebounce";

const ParamsPage = (props: any) => {
    const [data, setData] = useState<Array<any>>([]);
    const [page, setPage] = useState<number>(0);
    const [rowsPerPage, setRowsPerPage] = useState<number>(5);
    const [pages, setPages] = useState<number>(1);
    const [checked, setChecked] = useState<any>({});
    const [chartModificators, setChartModificators] = useState<Array<any>>([]);
    const [searchItem, setSearchItem] = useState<string>('');
    const [countChecked, setCountChecked] = useState<number>(0);
    const [isLoading, setLoading] = useState<boolean>(false);
    const debouncedSearchItem = useDebounce(searchItem, 500);
    const navigate: NavigateFunction = useNavigate();

    useEffect((): void => {
        loadData();
    }, [page, rowsPerPage]);

    useEffect((): void => {
        loadData();
        setPage(0);
    }, [debouncedSearchItem]);

    useEffect(() => {
        const newChecked: any = {};
        data.forEach((item) => {
            let id: string = item['indicatorId'] ?? item['code'];
            newChecked[id] = false;
        });
        setCountChecked(0);
        setChecked(newChecked);
    }, [pages]);

    const loadData = (): void => {
        setLoading(true);
        fetchContent(defineModificatorsURL(), {})
            .then((response) => {
                setData(response.responseBody);
                setPages(+response.responseMetadata.total);
            })
            .catch((err) => {
                console.log(err);
            })
            .finally(() => {
                setLoading(false);
            });
    }

    const defineModificatorsURL = (): string => {
        let path: string;
        if (props.mode !== Mode.Countries){
            path = `${server}api/indicatorsByCountry`;
            return createURL(path, {
                page: `${page + 1}`,
                perPage: `${rowsPerPage}`,
                countryId: props.queryId,
                indicatorNamePart: `${debouncedSearchItem}`
            });
        }
        path = `${server}api/countriesByIndicator`;
        return createURL(path, {
            page: `${page + 1}`,
            perPage: `${rowsPerPage}`,
            indicatorId: props.queryId,
            countryNamePart: `${debouncedSearchItem}`
        });
    }

    const handleClose = (): void => {
        props.onClose();
    }

    const handleDrawChart = (): void => {
        const ids = chartModificators.join(';');
        const chartURL = `/chart/?mode=${props.mode}&modificators=${ids}&id=${props.queryId}`;
        navigate(chartURL);
    }

    const handleChangePage = (
        event: React.MouseEvent<HTMLButtonElement> | null,
        newPage: number,
    ): void => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (
        event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
    ): void => {
        let newRowsPerPage = parseInt(event.target.value, 10);
        setPage(0);
        setRowsPerPage(newRowsPerPage);
    };

    const changeChecked = (id: string): boolean => {
        const isChecked = !checked[id];
        const updatedCheckState: any = Object.assign({}, checked);
        updatedCheckState[id] = !checked[id];
        setChecked(updatedCheckState);
        return isChecked;
    }

    const handleCheckboxChange = (name: string, id: string, checkedIdx: number): void => {
        const isChecked: boolean = changeChecked(id);
        const prev: any = chartModificators.slice();
        prev.push(id);
        const updatedChartModificators: any = isChecked ? prev :
            chartModificators.filter((itemId) => {
                return itemId !== id;
            });
        isChecked ? setCountChecked(countChecked + 1) : setCountChecked(countChecked - 1);
        setChartModificators(updatedChartModificators);
    }

    const defineHeader = (): JSX.Element => {
        return (
            <TableRow>
                <TableCell>
                    <TextField
                        onChange={(event) => {
                            setSearchItem(event.target.value);
                        }} label={`Search by name:`}
                    variant={"standard"}/> </TableCell>
            </TableRow>
        );
    }

    const defineIndex = (idx: number): number => {
        return (rowsPerPage * page) + idx + 1;
    }

    const defineData = () => {
        if (!data || data?.length === 0) {
            return (
                <TableRow>
                    <TableCell colSpan={1} align={'center'}>
                        <Typography> Nothing found </Typography>
                    </TableCell>
                </TableRow>
            );
        }
        return data.map((item: any, idx: number): any => {
            let id: string = item['indicatorId'] ?? item['code'];
            return (
                <TableRow>
                    <TableCell>
                        <FormControlLabel control={
                            <Checkbox
                                checked={checked[id]}
                                onChange={() => {
                                    let name: string = item['name'] ?? item['description'];
                                    handleCheckboxChange(name, id, defineIndex(idx) - 1);
                                }}
                            />
                        } label={item['name'] ?? item['description']}/>
                    </TableCell>
                </TableRow>
            );
        });
    }

    const isChartAllowed = (): boolean => {
        return countChecked > 0 && countChecked <= 5;
    }

    return (
        <Dialog
            open={true}
            onClose={handleClose}
            fullWidth={true}
        >
            <DialogTitle>
                {'Choose your modificators:'}
            </DialogTitle>
            <DialogContent>
                <TableContainer sx={{
                    width: "100%",
                    marginTop: '2rem'
                }} component={Paper}>
                    <Table  sx={{
                        width: "100%"
                    }}>
                        <TableHead>
                            {defineHeader()}
                        </TableHead>
                        <TableBody>
                            {  isLoading ? (
                                    <TableRow>
                                        <TableCell colSpan={1} align={'center'}>
                                            <CircularProgress/>
                                        </TableCell>
                                    </TableRow>
                                ) : defineData()  }
                        </TableBody>
                        <TableFooter>
                            <TableRow>
                                <TablePagination
                                    rowsPerPageOptions={[5, 10, 25]}
                                    colSpan={3}
                                    count={pages}
                                    rowsPerPage={rowsPerPage}
                                    page={page}
                                    SelectProps={{
                                        inputProps: {
                                            'aria-label': 'rows per page',
                                        },
                                        native: true,
                                    }}

                                    onPageChange={handleChangePage}
                                    onRowsPerPageChange={handleChangeRowsPerPage}
                                />
                            </TableRow>
                        </TableFooter>
                    </Table>
                </TableContainer>
            </DialogContent>
            <DialogActions>
                <Button disabled={!isChartAllowed()} onClick={handleDrawChart}> Draw chart </Button>
                <Button onClick={handleClose}> Cancel </Button>
            </DialogActions>
        </Dialog>
    );
}

export default ParamsPage;