import MainNavbar from "../MainNavbar";
import TableTab, {Mode} from "../tabs/TableTab";
import {Box, Tab, Tabs, Typography} from "@mui/material";
import React, {useState} from "react";
import ParamsPage from "./ParamsPage";

interface CurrentModificator {
    mode: string,
    id: string
}

const MainPage = (props: any) => {
    const [current, setCurrent] = useState<CurrentModificator>({
        mode: props.mode,
        id: ""
    });
    const [isParamsOpen, setIsParamsOpen] = useState<boolean>(false);

    const handleCurrent = (newCurrent: any): void => {
        setCurrent(newCurrent);
        setIsParamsOpen(true);
    }

    const handleParamsClose = (): void => {
        setIsParamsOpen(false);
    }

    return (
        <>
            <MainNavbar/>
            <Typography variant={'h2'} textAlign={'center'}> Table of contents: </Typography>
            { props.mode === Mode.Countries ?
                <TableTab handleCurrent={handleCurrent} mode={'countries'}/> :
                <TableTab handleCurrent={handleCurrent} mode={'indicators'}/> }
            { isParamsOpen && <ParamsPage
                        mode={current.mode}
                        queryId={current.id}
                        onClose={handleParamsClose}/> }
        </>
    );
}

export default MainPage;
export type { CurrentModificator };