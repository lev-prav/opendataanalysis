import {
    CircularProgress, Container,
    Grid,
    Typography
} from "@mui/material";
import ChartTab from "../tabs/ChartTab";
import ControlTab from "../tabs/ControlTab";
import React, {useEffect, useState} from "react";
import MainNavbar from "../MainNavbar";
import {useSearchParams} from "react-router-dom";
import {Mode} from "../tabs/TableTab";
import {server} from "../../index";
import {createURL, fetchContent} from "../../actions/httpActions";
import {getYearRange} from "../../actions/contentActions";
import useDebounce from "../../hooks/useDebounce";

export enum ChartMode {
    Line = 'line',
    Histogram = 'histogram',
    Table = 'table'
}

interface ChartColors {
    backgroundColor: string,
    borderColor: string
}

const ChartPage = (props: any) => {
    const [chartMode, setChartMode] = useState<ChartMode>(ChartMode.Line);
    const [data, setData] = useState<Array<any>>([]);
    const [isLoading, setLoading] = useState<boolean>(false);
    const [chartTitle, setChartTitle] = useState<string>('');
    const [minYear, setMinYear] = useState<number>(0);
    const [maxYear, setMaxYear] = useState<number>(0);
    const [curMinYear, setCurMinYear] = useState<number>(0);
    const [curMaxYear, setCurMaxYear] = useState<number>(0);
    //const [colors, setColors] = useState<Array<ChartColors>>([]);
    const debouncedCurMinYear = useDebounce(curMinYear, 500);
    const debouncedCurMaxYear = useDebounce(curMaxYear, 500);

    const [searchParams, setSearchParams] = useSearchParams();

    const mode = searchParams.get('mode');
    const modificators = searchParams.get('modificators');
    const identificator = searchParams.get('id');

    useEffect((): void => {
        loadData();
    }, []);

    const handleChartModeChange = (mode: ChartMode): void => {
        setChartMode(mode);
    }

    const generateColors = (data: any[]): void => {
        if (!Array.isArray(data)) {
            return;
        }

        data.forEach((dataset) => {
            dataset.borderColor = `rgb(${Math.random() * 256}, ${Math.random() * 256}, ${Math.random() * 256})`;
            dataset.backgroundColor = `rgba(${Math.random() * 256}, ${Math.random() * 256}, ${Math.random() * 256}, 0.5)`;
        });
    }

    const handleYearsChange = (updatedValue: number[]): void => {
        setCurMinYear(updatedValue[0]);
        setCurMaxYear(updatedValue[1]);
    }

    const definePath = (): string => {
        let path: string = `${server}api/infoByIndicator/`;
        if (mode !== Mode.Countries){
            path = `${server}api/infoByCountry/`;
            return createURL(path,
                {
                    'indicatorsId': `${modificators}`,
                    'countryId': `${identificator}`
                });
        }
        return createURL(path,
            {
                'countriesId': `${modificators}`,
                'indicatorId': `${identificator}`
            });
    }

    const loadData = (): void => {
        setLoading(true);
        fetchContent(definePath(), {})
            .then((content) => {
                setChartTitle(content.responseMetadata.indicatorDescription ?? content.responseMetadata.countryDescription)
                generateColors(content.responseBody);
                setData(content.responseBody);
                const {min, max} = getYearRange(content.responseBody);
                setMinYear(min);
                setMaxYear(max);
                setCurMinYear(min);
                setCurMaxYear(max);
            })
            .catch((err) => {
                console.log(err);
            })
            .finally(() => {
                setLoading(false);
            });
    }

    return (
        <>
            <MainNavbar/>
            <Container maxWidth={false}>
                <ControlTab
                    onChartModeChange={handleChartModeChange}
                    onYearsChanged={handleYearsChange}
                    minYear={minYear}
                    maxYear={maxYear}
                />

                {isLoading ? <CircularProgress style={{
                    margin: '10% 50%'
                }}/> : <ChartTab
                    content={data}
                    chartMode={chartMode}
                    chartTitle={chartTitle}
                    minYear={debouncedCurMinYear}
                    maxYear={debouncedCurMaxYear}
                />}
            </Container>
        </>
    );
}

export default ChartPage;