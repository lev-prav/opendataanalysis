import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableHead from "@mui/material/TableHead";
import TableBody from "@mui/material/TableBody";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import {TablePagination, Typography} from "@mui/material";
import TableContainer from "@mui/material/TableContainer";
import React, {useState} from "react";

const TableChart = (props: any) => {
    const [page, setPage] = useState<number>(0);
    const [rowsPerPage, setRowsPerPage] = useState<number>(5);
    const [pages, setPages] = useState<number>(props.content.length);

    const handleChangePage = (
        event: React.MouseEvent<HTMLButtonElement> | null,
        newPage: number,
    ): void => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (
        event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
    ): void => {
        let newRowsPerPage = parseInt(event.target.value, 10);
        setPage(0);
        setRowsPerPage(newRowsPerPage);
    };

    const defineHeader = () => {
        return (
            <TableRow>
                <TableCell> { props.modificator } </TableCell>
                {props.headers.map((item: string) => {
                    return (
                        <TableCell>
                            <Typography> { item } </Typography>
                        </TableCell>);
                })}
            </TableRow>
        );
    }

    const defineData = () => {
        let data = props.content;

        if (!data || data?.length === 0) {
            return (
                <TableRow>
                    <TableCell colSpan={props.headers.length + 1} align={'center'}>
                        <Typography> Nothing found </Typography>
                    </TableCell>
                </TableRow>
            );
        }

        data = rowsPerPage > 0 ? data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage) : data;

        return data.map((dataset: any) => {
            return (
                <TableRow>
                    <TableCell> { dataset.label } </TableCell>
                    {
                        dataset.data.map((item: any) => {
                            return (
                                <TableCell>
                                    <Typography> {item ? item : '-'} </Typography>
                                </TableCell>
                            )
                        })
                    }
                </TableRow>
            );
        });
    }

    return (
        <>
        <TableContainer sx={{
            width: "100%",
            margin: 'auto'
        }} component={Paper}>
            <Table>
                <TableHead>
                    { defineHeader() }
                </TableHead>
                <TableBody>
                    { defineData() }
                </TableBody>
            </Table>
        </TableContainer>
            <TablePagination
                rowsPerPageOptions={[5, 10, 25]}
                count={pages}
                rowsPerPage={rowsPerPage}
                page={page}
                component={"div"}
                SelectProps={{
                    inputProps: {
                        'aria-label': 'rows per page',
                    },
                    native: true,
                }}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
            />
        </>
    );
}

export default TableChart;