import React from 'react';
import {BrowserRouter, Route, Routes} from "react-router-dom";
import NotFound from "./pages/NotFound";
import MainPage from "./pages/MainPage";
import ChartPage from "./pages/ChartPage";

const App = () => {
    return (
        <BrowserRouter>
            <Routes>
                <Route path={'/'} element={<MainPage mode={'countries'}/>}/>
                <Route path={'/countries/'} element={<MainPage mode={'countries'}/>}/>
                <Route path={'/indicators/'} element={<MainPage mode={'indicators'}/>}/>
                <Route path={'/chart/'} element={<ChartPage/>}/>
                <Route path={'/*'} element={<NotFound/>}/>
            </Routes>
        </BrowserRouter>
    );
}

export default App;