# Disclaimer #

This project was created as part of the production practice of Fork Fam (formerly First Line Software). 

# OpenDataAnalysis #

This is a web application for displaying data from the World Bank (an organization that provides information on countries and their key indicators).

# Website Architecture #
![Website Architecture](Schemes/Site%20roots%20scheme.png)

1) First of all, the user must select the country or the indicator page from the header. At the same time, a list of all countries is displayed by default.
2) After that, a list is displayed with the current countries (if the indicator is selected at the previous stage) or indicators (if the country is selected at the previous stage).
> At this stage, the user is given the opportunity to select up to 5 countries/indicators for further drawing of graphs (this number is selected so as not to clog the graph)
3) On the chart, the user can view the data in the form of a line chart, pie chart or histogram.
> The user can remove countries by clicking on them in the chart legend
# Architecture #
![Architecture](Schemes/Project%20scheme.png)
## Console Application ##
For educational purposes, we have implemented our own database, which is filled with data by requesting it from the World Bank API and processing it directly in the console application. The processed data gets into the database using Bulk Copy technology.
> The console application works independently of the other parts
## Library for interacting with the DB ##
To interact with our database specifically, we use an intermediate layer in the form of a separate library, which contains methods for interacting with the database using the entity framework.
## Web Application ##
The web application uses previously written modules to get the necessary data from the database. After that, it converts the received data into the format required for the frontend.
>The application uses Swagger. The received documentation is expanded using XML comments.
## Database configuration ##
MS SQL Server was chosen as the database.

![Database Scheme](Schemes/Database%20Scheme.png)
>The "updatedAt" field in the "Indicators" table is required for the future when we implement the admin panel :)

# How to build the production version #
* First of all you need to build Frontend part of application (you can see instruction in the next clause)
* After that, go to the "OpenDataAnalysis" folder which match the web application folder and run it.

> If you want to fill database, go to the "FillDBApp" and run it (make sure that the database is created with the necessary schema, for this you need to run the web application at least once)

# How to run frontend part of project in development and production mode? #

## Development ##

(Please, check that the current version of node.js >= 10)

In the frontend directory run the command below:

`npm install && npm start`

## Production ##

To build a bundle in the frontend directory run the command below:

`npm install && npm build`

To run the application type this command:

`npm install -g serve && serve -s build`

If you encounter problems with privacy policy while running the app, check this [answer](https://stackoverflow.com/questions/63423584/how-to-fix-error-nodemon-ps1-cannot-be-loaded-because-running-scripts-is-disabl).

# How to use your own configure connection string: #

1) Create file with name "appsettings_{username}.json" in project directory near with appsettings.json

2) Write in
```
{  
    "ConnectionStrings": {  
        "Connection": "Server=localhost;Database=OpenDataAnalysisDb;Trusted_Connection=True;  MultipleActiveResultSets=true"
    }  
}
```
If you want to check your user name
For Windows 10: 

1) Open cmd (press win+r and write "cmd")  

2) Write "echo %username%" into cmd