﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OpenDataServiseLib.Models
{
    internal class ApplicationContext : DbContext
    {
        public DbSet<Country> Countries { get; set; }
        public DbSet<Indicator> Indicators { get; set; }
        public DbSet<CountryIndicator> CountryIndicators { get; set; }
        
        public ApplicationContext()
        {
            //    Database.EnsureCreated();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
           /* //BUG: USE YOUR OWN CONNECTION STRING
            optionsBuilder.UseSqlServer("Server=localhost;" +
                                        "Database=OpenDataAnalysisDb;" +
                                        "User ID=admin;Password=admin;  " +
                                        "MultipleActiveResultSets=true");*/
        }

        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options) { }

        public new async Task<int> SaveChanges()
        {
            return await base.SaveChangesAsync();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CountryIndicator>().
                   HasKey(k => new { k.CountryId, k.IndicatorId, k.Year });
        }

        public void EnsureCreated()
        {
            //Database.EnsureCreated();
        }
    }
}
