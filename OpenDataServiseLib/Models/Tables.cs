﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OpenDataServiseLib.Models
{
    public class Country
    {
        [Key]
        public string CountryId { get; set; }

        [Required]
        public string CountryName { get; set; }

        public ICollection<CountryIndicator> CountryIndicators { get; set; }
    }
    
    //тут была попытка добавить базовый класс Country и отнаследовать от него классы выше и ниже
    //но переопределение свойств ломало SQL
    public class CountryCount
    {
        public string CountryId { get; set; }
        public string CountryName { get; set; }
        public int IndicatorsCount { get; set; }
    }

    public class Indicator
    {
        [Key]
        public string IndicatorId { get; set; }

        [Required]
        public string IndicatorName { get; set; }
        
        [DataType(DataType.Date)]
        [Column(TypeName = "Date")]
        public DateTime UpdatedAt { get; set; } 

        public ICollection<CountryIndicator> CountryIndicators { get; set; }
    }
    public class IndicatorCount
    {
        public string IndicatorId { get; set; }
        public string IndicatorName { get; set; }
        public DateTime UpdatedAt { get; set; } 
        public int CountryCount { get; set; }
    }

    public class CountryIndicator
    {
        [ForeignKey("Country")]
        public string CountryId { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Country Country { get; set; }

        [ForeignKey("Indicator")]
        public string IndicatorId { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Indicator Indicator { get; set; }

        public int Year { get; set; }

        [Column(TypeName = "decimal(18,4)")]
        public decimal? Value { get; set; }
    }
}
