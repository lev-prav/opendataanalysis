﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace OpenDataServiseLib.Migrations
{
    public partial class DeleteEmptyData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DELETE FROM CountryIndicators " +
                                 "WHERE CountryIndicators.CountryId " +
                                 "NOT IN(SELECT Countries.CountryId FROM Countries) " +
                                  "OR CountryIndicators.IndicatorId " +
                                  "NOT IN(SELECT Indicators.IndicatorId FROM Indicators)");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
