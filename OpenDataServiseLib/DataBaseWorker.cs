﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using OpenDataServiseLib.Models;

namespace OpenDataServiseLib
{
    internal class DataBaseWorker : IDataBaseWorker
    {
        ApplicationContext appDb;
        public DataBaseWorker(ApplicationContext Dbcontext)
        {
            appDb = Dbcontext;
        }

        /// <summary>
        /// Метод возвращающий количество стран
        /// </summary>
        public async Task<int> GetCountriesCountAsync(string countryNamePart = "")
        {
            return await appDb.Countries
                .Where(c => c.CountryName.Contains(countryNamePart))
                .CountAsync();
        }

        /// <summary>
        /// Метод возвращающий количество индикаторов
        /// </summary>
        public async Task<int> GetIndicatorsCountAsync(string indicatorNamePart = "")
        {
            return await appDb.Indicators
                .Where(ind => ind.IndicatorName.Contains(indicatorNamePart))
                .CountAsync();
        }

        /// <summary>
        /// Метод возвращающий страны постранично 
        /// </summary>
        public async Task<List<CountryCount>> GetCountriesAsync(int page = 1, int perPage = 50, string countryNamePart = "")
        {
            return await appDb.Countries
                         .Where(c => c.CountryName.Contains(countryNamePart))
                         .Select(country => new CountryCount()
                         {
                             CountryId = country.CountryId, 
                             CountryName = country.CountryName,
                             IndicatorsCount = country.CountryIndicators.Select(ci => ci.IndicatorId).Distinct().Count()
                         })
                         .Where(cc => cc.IndicatorsCount > 0)
                         .Skip((page - 1) * perPage)
                         .Take(perPage)
                         .ToListAsync();
        }

        /// <summary>
        /// Метод возвращающий индикаторы постранично
        /// </summary>
        public async Task<List<IndicatorCount>> GetIndicatorsAsync(int page = 1, int perPage = 50, string indicatorNamePart = "")
        {
            return await appDb.Indicators
                .Where(ind => ind.IndicatorName.Contains(indicatorNamePart))
                .Select(indicator => new IndicatorCount()
                {
                    IndicatorId = indicator.IndicatorId, 
                    IndicatorName = indicator.IndicatorName,
                    UpdatedAt = indicator.UpdatedAt,
                    CountryCount = indicator.CountryIndicators.Select(ci => ci.CountryId).Distinct().Count()
                })
                .Skip((page - 1) * perPage)
                .Take(perPage)
                .ToListAsync();
        }

        /// <summary>
        /// Метод возвращающий количество индикаторов, у которых есть соответствующая страна
        /// </summary>
        public async Task<int> GetIndicatorsThatHaveCountryCountAsync(string countryId)
        {
            return await appDb.CountryIndicators
                .Where(c => c.CountryId == countryId)
                .GroupBy(i => i.IndicatorId)
                .CountAsync();
        }

        /// <summary>
        /// Метод возвращающий количество стран, у которых есть соответствующий индикатор
        /// </summary>
        public async Task<int> GetCountriesThatHaveIndicatorCountAsync(string indicatorId)
        {
            return await appDb.CountryIndicators
                .Where(i => i.IndicatorId == indicatorId)
                .GroupBy(g => g.CountryId)
                .CountAsync();
        }

        /// <summary>
        /// Метод возвращающий индикаторы, у которых есть соответствующая страна
        /// </summary>
        public async Task<List<Indicator>> GetIndicatorsThatHaveCountryAsync(string countryId, int page = 1, int perPage = 50, string indicatorNamePart = "")
        {
            return await appDb.CountryIndicators
                .Where(r => r.CountryId == countryId && r.Indicator.IndicatorName.Contains(indicatorNamePart))
                .GroupBy(i => i.IndicatorId)
                .Select(c => c.First().Indicator)
                .Skip((page - 1) * perPage)
                .Take(perPage)
                .ToListAsync();
        }

        /// <summary>
        /// Метод возвращающий страны, у которых есть соответствующий индикатор
        /// </summary>
        public async Task<List<Country>> GetCountriesThatHaveIndicatorAsync(string indicatorId, int page = 1, int perPage = 50, string countryNamePart = "")
        {
            return await appDb.CountryIndicators
                .Where(r => r.IndicatorId == indicatorId && r.Country.CountryName.Contains(countryNamePart))
                .GroupBy(g => g.CountryId)
                .Select(c => c.First().Country)
                .Skip((page - 1) * perPage)
                .Take(perPage)
                .ToListAsync();
        }

        /// <summary>
        /// Метод возвращающий количество строк с информацией по странам
        /// </summary>
        public async Task<int> GetInfoByIndicatorCountAsync(List<string> countriesId, string indicatorId)
        {
            return await appDb.CountryIndicators
                .Where(c => countriesId.Any(x => x == c.CountryId)
                        && indicatorId == c.IndicatorId)
                .CountAsync();
        }

        /// <summary>
        /// Метод возвращающий количество строк с информацией по индикаторам
        /// </summary>
        public async Task<int> GetInfoByCountryCountAsync(List<string> indicatorsId, string countryId)
        {
            return await appDb.CountryIndicators
                .Where(c => indicatorsId.Any(x => x == c.IndicatorId)
                        && countryId == c.CountryId)
                .CountAsync();
        }

        /// <summary>
        /// Метод возвращающий информацию по странам
        /// </summary>
        public async Task<List<CountryIndicator>> GetInfoByIndicatorAsync(List<string> countriesId, string indicatorId, int page = 1, int perPage = 50)
        {
            return await appDb.CountryIndicators
                .Select(x => new CountryIndicator
                {
                    CountryId = x.CountryId,
                    Country = x.Country,
                    IndicatorId = x.IndicatorId,
                    Indicator = x.Indicator,
                    Year = x.Year,
                    Value = x.Value,
                })
                .Where(c => countriesId.Any(x => x == c.CountryId)
                        && indicatorId == c.IndicatorId)
                .Skip((page - 1) * perPage)
                .Take(perPage)
                .ToListAsync();
        }

        /// <summary>
        /// Метод возвращающий информацию по индикаторам
        /// </summary>
        public async Task<List<CountryIndicator>> GetInfoByCountryAsync(List<string> indicatorsId, string countryId, int page = 1, int perPage = 50)
        {
            //TODO: добавить нужные поля
            return await appDb.CountryIndicators
                .Select(x => new CountryIndicator
                {
                    CountryId = x.CountryId,
                    Country = x.Country,
                    IndicatorId = x.IndicatorId,
                    Indicator = x.Indicator,
                    Year = x.Year,
                    Value = x.Value,
                })
                .Where(c => indicatorsId.Any(x => x == c.IndicatorId)
                        && countryId == c.CountryId)
                .Skip((page - 1) * perPage)
                .Take(perPage)
                .ToListAsync();
        }
    }
}
