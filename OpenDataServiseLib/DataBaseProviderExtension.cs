﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OpenDataServiseLib.Models;
using Microsoft.AspNetCore.Builder;

namespace OpenDataServiseLib
{
    public static class DataBaseProviderExtension
    {
        public static IServiceCollection AddFlsContext(this IServiceCollection services, string connectionString)
        {
            services.AddDbContext<ApplicationContext>(options =>
            {
                options.UseSqlServer(connectionString);
            });
            
            services.AddScoped<IDataBaseWorker, DataBaseWorker>();
            return services;
        }
        public static void EnsureDBCreated(this WebApplication app)
        {
            using (var serviceScope = app.Services.CreateScope())
            {
                var dbContext = serviceScope.ServiceProvider.GetRequiredService<ApplicationContext>();
                dbContext.EnsureCreated();
            }
        }
        public static void EnsureDBUpdated(this WebApplication app)
        {
            //TODO: Add method for db update
        }
    }
}
