﻿using OpenDataServiseLib.Models;

namespace OpenDataServiseLib
{
    public interface IDataBaseWorker
    {
        public Task<int> GetCountriesCountAsync(string countryNamePart = "");
        public Task<int> GetIndicatorsCountAsync(string indicatorNamePart = "");
        public Task<List<CountryCount>> GetCountriesAsync(int page = 1, int perPage = 50, string countryNamePart = "");
        public Task<List<IndicatorCount>> GetIndicatorsAsync(int page = 1, int perPage = 50, string indicatorNamePart = "");
        public Task<int> GetCountriesThatHaveIndicatorCountAsync(string indicatorId);
        public Task<int> GetIndicatorsThatHaveCountryCountAsync(string countryId);
        public Task<List<Country>> GetCountriesThatHaveIndicatorAsync(string indicatorId, int page = 1, int perPage = 50, string countryNamePart = "");
        public Task<List<Indicator>> GetIndicatorsThatHaveCountryAsync(string countryId, int page = 1, int perPage = 50, string indicatorNamePart = "");
        public Task<int> GetInfoByIndicatorCountAsync(List<string> countriesId, string indicatorId);
        public Task<int> GetInfoByCountryCountAsync(List<string> indicatorsId, string countryId);
        public Task<List<CountryIndicator>> GetInfoByIndicatorAsync(List<string> countriesId, string indicatorId, int page = 1, int perPage = 50);
        public Task<List<CountryIndicator>> GetInfoByCountryAsync(List<string> indicatorsId, string countryId, int page = 1, int perPage = 50);
    }
}
